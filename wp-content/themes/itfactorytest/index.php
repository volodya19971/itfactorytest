<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package itfactory
 */
get_header();
?>

<section class="container-fluid hot-trip" id="hot-trip">
    <div class="row">
        <div class="col-12">
            <h2 class="section-title text-center">Гарячі тури</h2>
            <p class="text-center section-sub__text">Lorem Ipsum is simply dummy text of the printing and typesettin industry</p>
        </div>
    </div>
    <div class="row">
        <?php
        $limit = 6;
        $args = array('category__in'=>'4',
            'numberposts' =>$limit);
        $posts = get_posts($args) ; ?>
        <?php  foreach( $posts as $post ):  ?>
            <div class="col-xl-3 col-lg-4 col-md-6">
                <div class="trip-item">
                    <div class="trip-item-rate">
                        <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                width="81px" height="13px">
                            <image  x="0px" y="0px" width="81px" height="13px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFEAAAANCAQAAAB7ykoxAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjCAEMJy1CyPhcAAAC6ElEQVRIx4WVTWxUVRTHf28+W6wtSgjIhwbBiIoKWjW1zFjbUlsGKYgEE5fu3LhT486lQV0RFxKNGBM/MMYu1LhwazDGRMcajW1nOkIhJgWDJH600/5c9HXmtfN8vXfzzju/+z/3nPvOfUhj5uy2I2K3zjbvtDORaHePXWto3Of1iUTaXtuadtS1wTHvTVx8i2d9IJHY6Uc+mEjc7Jj3JBKb/NytTTtFc2ziMXpIGts4xt5EYgdPsiuRuIsSOxOJfRS5o2lGtzhInuIaiwO6E4m7CdZIopf0GoUY5DpGm2aGLaT5G8jwFLCX+5kgT5YsM9SBFJtDop0ngB72USFHnhSXmA+JDH+xSBcjwEHOcJE0bQRcCjW2EPAPdTawHzjCaa4QkCfFRRaAgK3IHPNspBco8TrXCMiSwf1+bdUpp1xQ6553wqo/+7RB+DUUPWfVKavWI8S4J0IicMBvrDjptPOq/uakFX/0eENj0O+sOum0/6pac9KKZUcbxLDfW3XCmnOqTjtl1bKHMWuP37pynPdEpKey9vrDKuKCRyNEzoLlVUTNQyuIAX9aRVQ8ZD5CDDm+ivjFkvkl942eCrPTRT/19pgueyOskOonMcRm32wQi37srhbiJt9qEHU/9NaYKGfCGuqcH7g9eunssBa6Fnwo9irY5mxji/HXynavNTR6/kfjakj8aXcscZtXQmJ2WWO5owtsBP4AUoys6PPl0U8ncBWAUmwnHqQjJFIMxxIjdIZR1nEglihwA3AZWE9/+C7c/fvqWYueVMux/5gx9T37PKmOm44hvlDftc/X1HKjDaLzS/Vti55Sz5lt8ef9TH3HgqfVr2xvHvRux33V9WLOZ7xsX8viPVZ9xS4x57POWmwhdnvBl+0S23zOGQdjDvF3X7JDbPd5aw7FaMz4gh3iOl/016UoS66HHY3kdMAjLTV41MNmItbxFvkhH4/U9hFHW4gBj5oKnwP7LbUQBY81IgcOLyXxH6B1z79EPwaYAAAAAElFTkSuQmCC" />
                        </svg>
                        <div class="trip-item-likes">
                        <span>
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="11px" height="11px">
                            <image  x="0px" y="0px" width="11px" height="11px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAQAAAADpb+tAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjCAEMLw4odgMmAAAAfElEQVQI12XNwQoBURSA4b9hqWx4LFmJSMlrKikLlqzkBaYp1jRm9FvcO+7Ivztfp3MQEScevHl0GuaAI0tDpeOGM3em9maBe+YtLuxLBjzJSRU8iLdXVnG3cp1eDtxE3jpseObpu117di648OVvlUu8+N+1i9x5065D/QENhKZFmrGQZQAAAABJRU5ErkJggg==" />
                            </svg>
                            78
                        </span>
                            <span>
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="13px" height="11px">
                            <image  x="0px" y="0px" width="13px" height="11px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAALCAQAAAAOu8/qAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjCAEMLxjcorZ3AAAAf0lEQVQY022JMQ4BQQBF3w4RSldxDL1oXcAm2k00eiXhALQ6hXtoNlsQ/RxA8xSyZiPzfvP/f4ild19uHYo9Nz6trQy4tOVs391vVdiYuHb6ozAyJkcM1ORpcG6eBeI+I04WiMHjn7g4Evxm3REHB5IUzozq21X7JIUTb07T/gBiCb5SV65bfQAAAABJRU5ErkJggg==" />
                            </svg>
                            15
                        </span>
                        </div>
                    </div>
                    <img class="trip-item-picture" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="<?php echo get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true ); ?>">
                    <div class="trip-item-info">
                        <div class="trip-item-title">
                        <span class="trip-item-hotel">
                           <?php echo $post->post_title ?>
                        </span>
                            <br>
                            <hr>
                            <span class="trip-item-country">
                            <?php echo (get_post_meta($post->ID, 'country', true)); ?>
                        </span>
                            <span class="trip-item-price">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="17px" height="19px">
                            <image  x="0px" y="0px" width="17px" height="19px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAATCAQAAADcrC56AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjCAEMGTaJaCrNAAABF0lEQVQoz32SrUuDURTGnzmZg43hxDLWV2zCwOAfYFCDQRDFJqYViww0aDCJGGwrFoOgYhFEMGiwWN/gKypMZIIgDAx+oP4M79e5E31uuOd57q+cc66QOQV2GHYSIdfmeeGN6f+QAd6BT0b+RhYIdE+RHvK/kQwekVbopkExQXKkEPMkalHglr0EWaOXEk9YrdMGJgOkn2eq7IdPHw54Q06IReAiDk9YcqAZ0UfLiXYRm8YfipqxPlBHZE1vvjg1yDgegwgxFWePohmbM7LMEo3hIUzvFJevVJ1JH4T5UZeuFaihS1n54b0tJgD4ZqjjEywDcExaiA3gi0oHsgV4lKIFrAJjDpDminPKdtOjzJEySIU6maD+AcTOvxetwYNXAAAAAElFTkSuQmCC" />
                            </svg>
                            Від  <?php echo (get_post_meta($post->ID, 'price', true)); ?> $
                        </span>
                        </div>
                        <div class="trip-item-ad-info">
                            <div class="trip-item-date">
                                <span>Особи</span> - двоє дорослих <br>від  <?php echo (get_post_meta($post->ID, 'start-date', true)); ?>  -   <?php echo (get_post_meta($post->ID, 'finish-date', true)); ?>
                            </div>
                            <button class="btn-order">
                                Забронювати
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        <?php  endforeach ?>
    </div>
    <div class="show-more text-center">
        <a href="#" class="show-more-link" title="Показати більше">Показати більше пропозицій
            <svg
                    xmlns="http://www.w3.org/2000/svg"
                    xmlns:xlink="http://www.w3.org/1999/xlink"
                    width="8px" height="14px">
                <image  opacity="0.59" x="0px" y="0px" width="8px" height="14px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAOCAQAAAC4X5UdAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQfjCAERBDZi+nbyAAAAZUlEQVQI10XO0RGCMBAA0cfFEqzACNp/NzJoBZAW8CMkub/dudm7ZHFX9Akhm4dIduEpHE1QhNxUAkWSTY4m2C9VUq9drRh9J0wdX7LNdlP1Ivta60bDT/2UeSCTt8dAwulnHaf+BPAYZUkyADYAAAAASUVORK5CYII=" />
            </svg>
        </a>
    </div>
</section>

<?php

get_footer();
