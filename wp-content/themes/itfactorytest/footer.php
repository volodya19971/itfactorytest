<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package itfactory
 */

?>

</div>
<footer>
    <div class="container-fluid">
        <div class="row align-items-stretch">
            <div class="col-lg-4">
                <div>
                    <? wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'f-nav-panel')); ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div>
                    <ul class="contacts">
                        <li class="contacts-item f-contacts-item">
                            <i class="fas fa-map-marker-alt"></i>
                            <span class="contacts-item__text">
                                    Адреса: <br>
                                    м. Львів, вул. Кот лярська, 2
                                </span>
                        </li>
                        <li class="contacts-item f-contacts-item">
                            <i class="fab fa-skype"></i>
                            <span class="contacts-item__text">
                                    Skype: <br>
                                    flora2115
                                </span>
                        </li>
                        <li class="contacts-item f-contacts-item">
                            <i class="fas fa-phone"></i>
                            <span class="contacts-item__text">
                                    (032) 297 16-45 <br>
                                    (067)-756-58-76
                                </span>
                        </li>
                    </ul>
                    <div class="director-info">
                        <div class="director-picture">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/Photodirector.png" alt="Фото директора">
                        </div>
                        <div class="director-data">
                            Директор: <br>
                            Прізвище,ім'я,по-батькові
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="social-networks text-center">
                    <h4 class="social-networks-title">
                        Ми в соціальних мережах
                    </h4>
                    <p class="social-networks-sub__text">Знаходьте нас у соціальних мережах <br>  та підписуйтесь на цікаві новини</p>
                    <div class="social-networks-container">
                        <a href="#" class="network-item" title="Однокласники">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="39px" height="39px">
                                <image  x="0px" y="0px" width="39px" height="39px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAMAAAC7faEHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABpFBMVEX////////////////////////////////i4+SZnaJVXGM4QEg3P0dSWWGVmZ7f4eLGyMpIUFclLjdETFPBw8bS1NU7Q0vMztD3+PheZWxscnivsrWwtLdxd30mLzj09PXNz9KIjZLy8/P19faQlJn8/f2fo6dLU1rg4ePm5+hRWWCUmZ38/Pz7+/uLkJRmbHP5+fptc3mAhYqbn6NOVV3o6eru7++PlJjIy82TmJz+/v4oMDnAwsXz9PRXXmQsNT16gIWytbguN0BNVVzw8fLJy84zO0QrNDwuNj/BxMe2ubw1Pkavs7b4+Pne3+B/hIlDS1IwOUFBSVF6f4Xb3d60t7piaG/Hyszn6OnNz9FKUVmsr7NASFAtNT6Ahou8v8LX2Nrl5ufY2duDiI0vOEA8REx9gohQV15RWF92fIKgpKianqLw8PFTWmExOUIpMjuMkZbV19n9/f3x8vJpcHZfZmyeoqYqMzyRlZooMTr6+vo9RU1CSlKNkpc/R0/R09VGTlY+Rk7P0dOjpqrExsnO0NJ4foNhZ27HyctbYmhXXmXZ2twM/ULYAAAAB3RSTlMGke+QiO2JZ9hXMwAAAAFiS0dEAIgFHUgAAAAHdElNRQfjCAENOB4xgP3jAAABiUlEQVQ4y83U2TtCQRQA8Ftk6hZykC2KuhKllCXJki072fclIqJys5N990+be/N5m+k+eHAe5jsPv+98Z86cbxhGJkeZQi5jmKyMSohsRiHJKRhJDKF/45QqVq3JzcvP4LQFIEZhEdUV6+AnNCUUV1qGRXmFvrIKgDWQnRGz6hqETGacaImOqwWw1AmZtR6gwUZy9kYAh0nInE0ALiexnhuguUXIWlmANjuxPw9uq92LE3cHgI98j84ugO4evd/VC9DXT5nfwCCuGAjgY2iY+h4j6vSYR8do72EaRxOTU8Hg9Mwscpq8JDdnmV/AQ1u0ehG35FheIblVgLX1jc0ta8i3vQMQJrldsTXdXiQsJvskZzj4XRcAdfSQfN+Q+SgWB4jHIolj6lz4pPUE4FSZ5BDVIXSG3+L8gkd0x6suxe4SV3QXxQtwfYNhiqM4sdqt5+5eqGgnOt6P7/qA9/1RgE/EfbalMHsWshcMX4n7jJRv7Hs6M358fhH3gBJ/7qT+p9L+5xxG4n//DR3JXiaGfT9zAAAAAElFTkSuQmCC" />
                            </svg>
                        </a>
                        <a href="#" class="network-item" title="Google+">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="39px" height="39px">
                                <image  x="0px" y="0px" width="39px" height="39px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAMAAAC7faEHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABm1BMVEX////////////////////////////////+/v6wtLdka3FCSlIuNj8lLjc0PUVQV15+g4nW2Nnk5udpcHZAR0+YnKD19fbf4eJXXmVHT1bl5uft7u9dZGszO0RPVl1obnRjaW8qMzz9/f35+fr4+PmWmp/Jy878/Pzx8vKhpalRWWDO0NLV19mBh4x3fYLg4ePn6Ok5QUltc3ny8/Pz9PS3ur0pMjurr7JJUFjIy803P0fc3d9zeX6anqKgpKidoaWfo6ekqKzw8PHa3N2+wcPFx8qUmZ0xOULBw8a7vsHKzM4tNT7AwsXBxMcoMTorNDzU1tiCh4wyOkMuN0AmLzgvOEBFTVVFTFQ2PkfR09V9gogoMDk+Rk5OVV3CxccsNT3Y2dvZ29y9wMPGyMqSl5ueoqa7vcCkp6vf4OGIjZKFio+HjJGJjpM9RU3i4+TZ2txVXGPw8fJfZmzv7/BLUlpudHpjanDb3d6Gi5B4foPh4uT7+/vu7+/m5+hLU1o6QkpnbXODiI2AhYqMkJX8/f1ETFNgZ23s7e5la3JgDYBGAAAAB3RSTlMGke+QiO2JZ9hXMwAAAAFiS0dEAIgFHUgAAAAHdElNRQfjCAENOC2OUJz1AAABj0lEQVQ4y2NgYGRiJwSYGBkYmAmqAgEWBlai1LEyEKWMnX3EquPg5OLm4eXl4xcQxKdOSBioCAJERMVwqhOX4EUASSlc6qRlQPKycvIK3Iq8koIcUEklZRUlFHWqIGUKaurs7BqaWtpwQ3R09fSR1RkYApUZGUNETBCWmZrxqiGrMwcqsxDCdJSlFa81sjoboDpbEEPKzt4BBOwdndidXVzd3Hk1PTy94Oq0eHm9fUAMXzM/MxDw8w9gDwziCw7hDZV1D4OrC+fljQDr8oWHTSR7lJ9ZNC9vtJ9fDFxdLC9vcByI4QNWE8LLG5/AnpiUnOLOm+qTZglXZwSUTAcxMjKzsnOigMbk5oG4rhG8+cj+0ASqKyiE+RJkaFExOFz8UMNFPwgopQsN+uISIMcNzNEuLStHVsdRAZSKrqwCscu5gGwrSJxUF9aoo8RbbR3I/fUNjU0K9SBWcwuO9KLdipReeNvacaa/SETCiu4Qx51O2QvdCkC+4Y2QcFBhx6OOABhx6ogtT4krn9kYiCzvAdb3XUuuAYfsAAAAAElFTkSuQmCC" />
                            </svg>
                        </a>
                        <a href="#" class="network-item" title="Facebook">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="39px" height="39px">
                                <image  x="0px" y="0px" width="39px" height="39px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAMAAAC7faEHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAA51BMVEX////////////////////////////////8/f3CxcdvdHo/R08wOUExOUI4QEhLU1qfo6cqMzwlLjcoMTrExsktNT4sNT39/f1udHrl5udOVV00PESVmZ7HycvFx8rHyszT1ddCSlJxd33R09VAR0+HjJHS1NVBSVGGi5Db3d6MkJWeoqaMkZaTmJx4foMyOkNTWmGWmp+Ok5ehpak3P0dRWWBASFB1e4E9RU1RWF88REw+Rk4oMDkvOEA7Q0usr7Pf4eLZ29zf4OG1uLt2fILh4uTX2Nry8/PZ2tyKj5TQ0tSCh4xZYGdm5TlKAAAAB3RSTlMGke+QiO2JZ9hXMwAAAAFiS0dEAIgFHUgAAAAHdElNRQfjCAENODdzMmWPAAAA1UlEQVQ4y+3UtxLCMBAEUJlkMtgEE0w0YHLGgMk5/v/3IEEBBdypoKBg6zejudXMEiJYRCwWgRArqlhsxM7l7ISLieIPOafL7fH6/AHMBSWZJYS4cOTO5CjslNiDyRLs4glqkmoqnYFdNkddXkHv1QrUFfFeSjp1ZdxV2LtVxNXqjWaLunanW+0Bri8/MwCc8eKGgBu9uDrgxuakP6Vm5jHnC8AtV+uNSd12t9of4F6OrJcT3p9m8PX8d+/dmf3xBXf7q6nrKu4+5OuOd0/59tlBOPf+BiT9POIFJGeQAAAAAElFTkSuQmCC" />
                            </svg>
                        </a>
                        <a href="#" class="network-item" title="Вк">
                            <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                    width="39px" height="39px">
                                <image  x="0px" y="0px" width="39px" height="39px"  xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAnCAMAAAC7faEHAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABUFBMVEX////////////////////////////////i4+TO0NLNz9LNz9HKzM7R09X19fbP0dNhZ25ETFM7Q0s5QUk8RExLUlqYnaH7+/v29/fY2dvU1tjT1dfj5OVRWF8lLjeMkZb9/f3MztBXXmVASFDV19n+/v6ZnaImLzhLU1qCh4w0PUXf4OHg4eM+Rk7p6utBSVFjanCOk5dfZmxDS1Klqa2coKQzO0T8/f1GTlbZ2tzs7e4pMju7vcDv7/CAhYpIUFfb3d6JjpOGi5Cmqq3S1NVdZGtKUVne3+AwOUFTWmFcYmnw8fLh4uRAR09QV17c3d8sNT1ka3F5foQrNDxscnhXXmS0t7r3+PhrcXf5+fpFTVXk5eYyOkNdY2rn6OmMkJUnMDlYX2Zxd32qrrFPVl1WXWR6gIUqMzy8v8JobnSjpqrZ29xUW2Lm5+hbYmhRWWCM11OPAAAAB3RSTlMGke+QiO2JZ9hXMwAAAAFiS0dEAIgFHUgAAAAHdElNRQfjCAENOQWi/gVOAAABXElEQVQ4y2NgYGRiJwSYGBkYmAmqAgEWBlai1LEyEKWMnX1UHR3VcXBycfPw8gFZ/AKCQsIiomLiIGEJSSlpfi5+Gbg6WTkQkFdgZ1dUAjOVVYCiqmpy6upAjgZcnSZYUksbyNQRBDF1+YFMPX2wsLoBXJ0ORMQQZJcRiGUsBWSagAXlNLgQ/jAFi5jpsLObG4BYFpbs7FbKYEFrGyT/2oJtk7ODqbN3YGd3hBjnhBIuzhADXdj5XEEMN3d2Hg+wkBEfijpbT7Col7ePKIj2lbbyAwsI+aOFs00AWDwwKBjsx5BQMNcjDD0+woPksICISIx444jCok4jGjN+w2KwKIyNw1CnEK+OpCABSidGY6QX8SSEMqPkFCgrVQ8jXfGlpUMlAzLY3d2g7EwdzPSXZQGR03VhZ3dRhinEkk6zDXNA8RCaC2Rz5UHU5dMs3Q9mdcSWp8SVz2wMRJb3AG61XFj/cBglAAAAAElFTkSuQmCC" />
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright text-center">
            © 2017 oTITOUR, ТУРОПЕРАТОР
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('.nav-bars').click(function (e) {
            e.preventDefault();
            $('.nav-mobile').toggleClass('active');
        });
        if($(window).width()<1200){
            let headerHeight =$('.nav_header').outerHeight();
            $('.nav-mobile').css('height',`calc(100vh - ${headerHeight}px)`);
        }

        $('.carousel').carousel({
            interval: 200000000
        })
    });

</script>
</body>
</html>
