<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package itfactory
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/css/style.css">
    <?php wp_head(); ?>
</head>

<body>
    <header>
        <div class="container-fluid nav_header">
            <nav>
                <div class="row align-items-center">
                    <div class="col-lg-2 col-sm-3 col-3 logo">
                        <a href="#" title="Логотип">
                            <img src="<?php echo get_template_directory_uri();?>/assets/img/logo.png" alt="">
                        </a>
                    </div>
                    <div class="col-lg-10 col-sm-9 col-9  d-flex d-xl-none justify-content-end">
                        <a href="#" class="nav-bars" title="Показати меню">
                            <i class="fas fa-bars"></i>
                        </a>
                    </div>
                    <div class="col-xl-10 col-12 nav-mobile">
                        <div class="d-xl-flex align-items-center justify-content-between">
                            <? wp_nav_menu(array('menu' => 'additional-menu', 'menu_class' => 'nav-panel main-info justify-content-start')); ?>
                            <ul class="contacts d-xl-flex align-items-center">
                                <li class="contacts-item">
                                    <i class="fas fa-map-marker-alt"></i>
                                    <span class="contacts-item__text">
                                    Адреса: <br>
                                    м. Львів, вул. Котлярська, 2
                                </span>
                                </li>
                                <li class="contacts-item">
                                    <i class="fab fa-skype"></i>
                                    <span class="contacts-item__text">
                                    Skype: <br>
                                    flora2115
                                </span>
                                </li>
                                <li class="contacts-item">
                                    <i class="fas fa-phone"></i>
                                    <span class="contacts-item__text">
                                    (032) 297 16-45 <br>
                                    (067)-756-58-76
                                </span>
                                </li>
                            </ul>
                        </div>
                        <hr>
                        <div>
                            <? wp_nav_menu(array('menu' => 'main-menu', 'menu_class' => 'nav-panel main-info justify-content-between')); ?>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <div id="carouselExampleIndicators" class="carousel slide" >
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="carousel-caption">
                        <h1 class="main-title text-center">Нові горизонти Вашого відпочинку</h1>
                        <p class="main-sub__text text-center">Ми поставили перед собою завдання зробити якісний відпочинок доступним для<br> всіх українських сімей і розробили нову концепцію щасливого і безтурботного<br> відпочинку з дітьми</p>
                    </div>
                </div>
                <div class="carousel-item ">
                    <div class="carousel-caption">
                        <h2 class="main-title text-center">Нові горизонти Вашого відпочинку</h2>
                        <p class="main-sub__text text-center">Ми поставили перед собою завдання зробити якісний відпочинок доступним для<br> всіх українських сімей і розробили нову концепцію щасливого і безтурботного<br> відпочинку з дітьми</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="carousel-caption ">
                        <h2 class="main-title text-center">Нові горизонти Вашого відпочинку</h2>
                        <p class="main-sub__text text-center">Ми поставили перед собою завдання зробити якісний відпочинок доступним для<br> всіх українських сімей і розробили нову концепцію щасливого і безтурботного<br> відпочинку з дітьми</p>
                    </div>
                </div>
            </div>
        </div>
    </header>




