<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'itfactorytest' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '+n q]6{vPVXdmc/y/Hc}$cT2II{IJi5&VCLzv_KOYOK$`y~a1eMG@VciXK<x%Man' );
define( 'SECURE_AUTH_KEY',  'u=Hmb|0K?tU~Nx6wmwotY)izf0.GSv- eL[WRCGr]gycJRk%C6vBd|vDYXNLa{?Z' );
define( 'LOGGED_IN_KEY',    'zV5m9dUw3 }q5;[/@Mt~vC)lVO`U<Y+Hc;HePyd8!2`Y?$aXR/*+Cc_x&q2EHpJm' );
define( 'NONCE_KEY',        'J(qOd6G!;rcC,X>gins`d/hwlE-n9b6aM~O9P)5:N0yBU<OdaUe~D,hh_uL?UwQ8' );
define( 'AUTH_SALT',        '+ l[*#0w3*#,6VZ>{Ur-a.;^A)MDj:4B9I[Pzk 7B{+ZUDVCW SZpM%1n-tk|^7i' );
define( 'SECURE_AUTH_SALT', '4[uZN|tK 0/7g&$N)*L*2}TdwVZ<hf _Sm6bO6g|q-ZM<;v)J7BL,l9.sW(4v:</' );
define( 'LOGGED_IN_SALT',   '*roS=rQh6?]6f8EnoH.*R!|f[_E~_Q5]g=:#Slw=w^;,6FgT>]j[vfC=5R|bDEm=' );
define( 'NONCE_SALT',       'B0DyG76HR`6R&L>nF,$dow&N0sE.GX&}z~SUI,&V~o0@glxK)(%@_4!HL&-]:7fq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
